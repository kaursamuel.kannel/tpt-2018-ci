create ci:


```

stage: install
   job that creates deps.txt where is placed random text  `echo "text" > deps.txt`
      place this deps.txt into artifact and cache it
      
stage test

   test job that will download deps.txt artifact and echos it out (`cat deps.txt`)
   
   test job that wil run following command
      - `date > date.txt`
      - `cat date.txt`
      date.txt will be placed into artifact
      
stage build:

   single job that will download install artifact and creates files build.txt and places into artifact
   
stage deploy:

   single job that echos build.txt ( only downloads build artifact )
     - this job is manually started  
     
```